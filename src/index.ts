/**
 * Represents optional values by extending the notion of a nullable type with
 * methods for operating on the underlying value (if any).
 *
 * Instances of `Option` are either an instance of `Some` or `None`.
 *
 * `Some` represents a defined value while `None` represents the absence of one.
 */
interface IOption<A> {

  /**
   * Get the underlying value from an instance of `Some` or `undefined`.
   */
  get: A | undefined;

  /**
   * `true` when this is a `Some`, `false` otherwise.
   */
  isDefined: boolean;

  /**
   * Get the underlying value from an instance of `Some` or fall back
   * to a default value if this is `None`.
   * @param fallback Default value.
   * @returns The underlying value or the result of the `fallback` function.
   */
  getOrElse(fallback: A): A;

  /**
   * Get the underlying value from an instance of `Some` or fall back
   * to a default value if this is `None`.
   * @param fallback Function that returns the default value.
   * @returns The underlying value or the `fallback`.
   */
  getOrElseLazy(fallback: () => A): A;

  /**
   * Apply the given function to the underlying value of an instance of `Some`,
   * returning a new `Option` that contains the result. Does nothing if this is
   * `None`.
   * @param f Function to apply to the underlying value.
   * @returns `Some` containing the application of `f` or `None`.
   */
  map<B>(f: (a: A) => B): Option<B>;

  /**
   * Apply the given function to the underlying value of an instance of `Some`,
   * returning the result. Does nothing is this is `None`.
   * @param f Function to apply to the underlying value.
   * @returns The application of `f` or None`.
   */
  flatMap<B>(f: (a: A) => Option<B>): Option<B>;

  /**
   * Apply the given function to the underlying value of an instance of `Some`,
   * returning the result. Falls back to a default value if this is `None`.
   * @param fallback Default value.
   * @param f Function to apply to the underlying value.
   * @returns The application of `f` or the `fallback`.
   */
  fold<B>(fallback: B): (f: (a: A) => B) => B;

  /**
   * Apply the given function to the underlying value of an instance of `Some`,
   * returning the result. Falls back to a default value if this is `None`.
   * @param fallback Function that returns the default value.
   * @param f Function to apply to the underlying value.
   * @returns The application of `f` or the result of the `fallback` function.
   */
  foldLazy<B>(fallback: () => B): (f: (a: A) => B) => B;
}

export interface Some<A> extends IOption<A> {
  get: A;
  isDefined: true;
}

export interface None<A = never> extends IOption<A> {
  get: undefined;
  isDefined: false;
}

export type Option<A> = Some<A> | None<A>;

class SomeImpl<A> implements Some<A> {
  readonly isDefined = true;

  constructor(readonly get: A) {}

  getOrElse(): A {
    return this.get;
  }

  getOrElseLazy(): A {
    return this.get;
  }

  map<B>(f: (a: A) => B): Some<B> {
    return new SomeImpl(f(this.get));
  }

  flatMap<B>(f: (a: A) => Option<B>): Option<B> {
    return f(this.get);
  }

  fold<B>(): (f: (a: A) => B) => B {
    return f => f(this.get);
  }

  foldLazy<B>(): (f: (a: A) => B) => B {
    return this.fold();
  }
}

class NoneImpl implements None<any> {
  readonly isDefined = false;
  readonly get = undefined;

  getOrElse<A>(fallback: A): A {
    return fallback;
  }

  getOrElseLazy<A>(fallback: () => A): A {
    return fallback();
  }

  map<B>(): Option<B> {
    return this;
  }

  flatMap<B>(): Option<B> {
    return this;
  }

  fold<B>(fallback: B) {
    return () => fallback;
  }

  foldLazy<B>(fallback: () => B) {
    return () => fallback();
  }
}

/**
 * Create a defined optional value `Some(value)`.
 * @param value Underlying value.
 * @returns `Some(value)`.
 */
export const some = <A>(value: A): Some<A> => new SomeImpl(value);

/**
 * An instance of `None`.
 */
export const none: None = new NoneImpl();

/**
 * Accepts a condition `cond` and a function `then`, returning `Some(then())`
 * when `cond` evaluates to true. Otherwise, `None` is returned.
 * @param cond Condition to evaluate.
 * @param then Function invoked when `cond` is true.
 * @returns `Some(then())` when `cond` is `true`, `None` otherwise.
 */
export const when = <A>(cond: boolean, then: () => A): Option<A> =>
  cond ? new SomeImpl(then()) : none;
