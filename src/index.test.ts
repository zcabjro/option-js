import { Option, some, none, when } from '.';

describe('Option', () => {
  describe('when', () => {
    it('returns Some when condition is true', () => {
      const opt = when(true, () => 1);
      expect(opt).toEqual(some(1));
    });

    it('returns None when condition is false', () => {
      const opt = when(false, () => 1);
      expect(opt).toEqual(none);
    });
  });

  describe('Some', () => {
    describe('get', () => {
      it('returns the value', () => {
        expect(some(1).get).toEqual(1);
        expect(some("abc").get).toEqual("abc");
        const array: unknown[] = [];
        expect(some(array).get).toEqual([]);
        expect(some(array).get).toBe(array);
        const obj = Object.freeze({});
        expect(some(obj).get).toBe(obj);
      });
    });

    describe('getOrElse', () => {
      it('returns the value', () => {
        expect(some(1).getOrElse(2)).toEqual(1);
        expect(some('abc').getOrElse('def')).toEqual('abc');
        const array: unknown[] = [];
        expect(some(array).getOrElse([1, 2, 3])).toEqual([]);
        expect(some(array).getOrElse([1, 2, 3])).toBe(array);
        const obj = Object.freeze({});
        expect(some(obj).getOrElse({ a: 'a'})).toEqual({});
        expect(some(obj).getOrElse({ a: 'a'})).toBe(obj);
      });
    });

    describe('getOrElseLazy', () => {
      it('returns the value', () => {
        expect(some(1).getOrElseLazy(() => 2)).toEqual(1);
        expect(some("abc").getOrElseLazy(() => 'def')).toEqual("abc");
        const array: unknown[] = [];
        expect(some(array).getOrElseLazy(() => [1, 2, 3])).toEqual([]);
        expect(some(array).getOrElseLazy(() => [1, 2, 3])).toBe(array);
        const obj = Object.freeze({});
        expect(some(obj).getOrElseLazy(() => ({ a: 'a'}))).toEqual({});
        expect(some(obj).getOrElseLazy(() => ({ a: 'a'}))).toBe(obj);
      });
    });

    describe('map', () => {
      it('applies the given function to the value', () => {
        const f = jest.fn<number, [number]>().mockReturnValue(2);
        const option = some(1);
        expect(option.map(f)).toEqual(some(2));
        expect(f).toHaveBeenCalledTimes(1);
        expect(f).toHaveBeenCalledWith(1);
      });
    });

    describe('flatMap', () => {
      it('applies the given function to the value', () => {
        const mockValue = some('abc');
        const f = jest
          .fn<Option<string>, [number]>()
          .mockReturnValue(mockValue);

        const option = some(1);
        expect(option.flatMap(f)).toEqual(mockValue);
        expect(f).toHaveBeenCalledTimes(1);
        expect(f).toHaveBeenCalledWith(1);
      });
    });

    describe('fold', () => {
      it('applies the given function to the value', () => {
        const mockValue = 'abc';
        const f = jest
          .fn<string, [number]>()
          .mockReturnValue(mockValue);

        const option = some(1);
        expect(option.foldLazy<string>(() => '')(f)).toEqual(mockValue);
        expect(f).toHaveBeenCalledTimes(1);
        expect(f).toHaveBeenCalledWith(1);
      });
    });

    describe('foldLazy', () => {
      it('applies the given function to the value', () => {
        const mockValue = 'abc';
        const f = jest
          .fn<string, [number]>()
          .mockReturnValue(mockValue);

        const option = some(1);
        expect(option.foldLazy<string>(() => '')(f)).toEqual(mockValue);
        expect(f).toHaveBeenCalledTimes(1);
        expect(f).toHaveBeenCalledWith(1);
      });
    });
  });

  describe('None', () => {
    describe('get', () => {
      it('returns undefined', () => {
        expect(none.get).toEqual(undefined);
      });
    });

    describe('getOrElse', () => {
      it('returns the fallback value', () => {
        const fallback = 1234;
        const option: Option<number> = none;
        expect(option.getOrElse(fallback)).toEqual(fallback);
      });
    });

    describe('getOrElseLazy', () => {
      it('returns the fallback value', () => {
        const mockValue = 1;
        const f = jest.fn<number, []>().mockReturnValue(mockValue);

        const option: Option<number> = none;
        expect(option.getOrElseLazy(f)).toEqual(mockValue);
        expect(f).toHaveBeenCalledTimes(1);
      });
    });

    describe('map', () => {
      it('returns None', () => {
        expect(none.map(() => 1)).toBe(none);
      });
    });

    describe('flatMap', () => {
      it('returns None', () => {
        expect(none.flatMap(() => some(1))).toBe(none);
      });
    });

    describe('fold', () => {
      it('returns the fallback value', () => {
        expect(none.fold<string>('abc')(() => '')).toEqual('abc');
      });
    });

    describe('foldLazy', () => {
      it('returns the fallback value', () => {
        const mockValue = 'abc';
        const f = jest
          .fn<string, []>()
          .mockReturnValue(mockValue);

        expect(none.foldLazy<string>(f)(() => '')).toEqual(mockValue);
        expect(f).toHaveBeenCalledTimes(1);
      });
    });
  });
});
