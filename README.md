# @zcabjro/option

- [Summary](#summary)
- [Installation](#installation)
- [Overview](#overview)
- [Helpers](#helpers)
- [Examples](#examples)
  - [get](#get)
  - [isDefined](#isdefined)
  - [getOrElse](#getorelse)
  - [getOrElseLazy](#getorelselazy)
  - [map](#map)
  - [flatMap](#flatmap)
  - [fold](#fold)
  - [foldLazy](#foldlazy)
  - [when](#when)

## Summary

Represents optional values by extending the notion of a nullable type with methods for operating on the underlying value (if any).

Instances of `Option` are either an instance of `Some` or `None`.

`Some` represents a defined value while `None` represents the absence of one.

Inspired by Scala's `Option` type.

## Installation

```sh
npm install @zcabjro/option
```

## Overview

| Property                        | `Some`                                                             | `None`                                       |
|---------------------------------|--------------------------------------------------------------------|----------------------------------------------|
| [get](#get)                     | Get the underlying value                                           | `undefined`                                  |
| [isDefined](#isDefined)         | `true`                                                             | `false`                                      |
| [getOrElse](#getorelse)         | Gets the underlying value                                          | Falls back to a given value                  |
| [getOrElseLazy](#getorelselazy) | Gets the underlying value                                          | Falls back to a given lazily evaluated value |
| [map](#map)                     | Applies a function to the underlying value                         | noop                                         |
| [flatMap](#flatmap)             | Applies a function (that returns `Option`) to the underlying value | noop                                         |
| [fold](#fold)                   | Applies a function to the underlying value and returns the result  | Falls back to a given value                  |
| [foldLazy](#foldlazy)           | Applies a function to the underlying value and returns the result  | Falls back to a given lazily evaluated value |

## Helpers

| Helper        | Summary                                                                                           |
|---------------|---------------------------------------------------------------------------------------------------|
| [when](#when) | Accepts a condition `C` and a value `V`, returning `Some` when `C` is true or `None` otherwise |

## Examples

### `get`

Gets the underlying value when defined or throws.

```js
some("Alice").get // "Alice"
none.get          // undefined
```

### `isDefined`

Returns `true` for `Some` and `false` for `None`.

```js
some("Alice").isDefined // true
none.isDefined          // false
```

### `getOrElse`

Gets the underlying value when defined or falls back to a given default value.

```js
some("Alice").getOrElse("Bob") // "Alice"
none.getOrElse("Bob")          // "Bob"
```

### `getOrElseLazy`

Gets the underlying value when defined or falls back to a given default, lazyily evaluated value.

```js
some("Alice").getOrElseLazy(() => "Bob") // "Alice"
none.getOrElseLazy(() => "Bob")          // "Bob"
```

### `map`

Applies the given function to the underlying value when defined, returning a new `Option` with the result.

```js
some("Alice").map(s => s.length) // Some(5)
none.map(noop)                   // None
```

### `flatMap`

Applies the given function to the underlying value when defined, returning the result.

```js
some("Alice").flatMap(s => some(s.length)) // Some(5)
some("Alice").flatMap(() => none)          // None
none.flatMap(() => some("not executed"))   // None
none.flatMap(() => none)                   // None
```

### `fold`

Applies the given function to the underlying value when defined, returning the result.

Falls back to a given default value otherwise.

```js
some("Alice").fold(0)(s => s.length) // 5
none.fold(0)(() => 5)                // 0
```

### `foldLazy`

Applies the given function to the underlying value when defined, returning the result.

Falls back to a given default, lazily evaluated value otherwise.

```js
some("Alice").foldLazy(() => 0)(s => s.length) // 5
none.foldLazy(() => 0)(() => 5)                // 0
```

### `when`

Returns `Some` when the given condition is true. Otherwise it returns `None`.

```js
when(true, () => "Alice")  // Some("Alice")
when(false, () => "Alice") // None
```
